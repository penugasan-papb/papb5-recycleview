package com.example.tugas5papb_

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tugas5papb_.data.entity.User

class ItemsAdapter(var list: List<User>)
    : RecyclerView.Adapter<ItemsAdapter.ItemsViewHolder>() {

    private lateinit var dialog: Dialog

    fun setDialog(dialog: Dialog){
        this.dialog = dialog
    }
    interface Dialog{
        fun onClick(position: Int)
    }

    inner class ItemsViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val profile: ImageView
        val nama: TextView
        val nim: TextView

        init {
            profile = view.findViewById(R.id.profile)
            nama = view.findViewById(R.id.nama)
            nim = view.findViewById(R.id.nim)
            view.setOnClickListener {
                dialog.onClick(layoutPosition)
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsViewHolder {

        return ItemsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent,false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }


    override fun onBindViewHolder(holder: ItemsViewHolder, position: Int) {
        holder.nama.text = list[position].fullName
        holder.nim.text = list[position].nim
        holder.profile.setImageResource(R.drawable.gambar)
    }
}


